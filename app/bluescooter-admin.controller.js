var blueScooterAdminModule = angular.module('bluescooter-admin');

blueScooterAdminModule.controller('main', function($scope, $location) {

    $scope.tabs = [
        {
            'name': 'Category',
            'path': '/'
        },
        {
            'name': 'Product',
            'path': '/product'
        },
        {
            'name': 'Order',
            'path': '/order'
        }
    ]
});
