var blueScooterAdminModule = angular.module('bluescooter-admin');


blueScooterAdminModule.controller('orderCtrl', function($scope, $http, $httpParamSerializerJQLike) {

        var fetchOrders = function() {
            $scope.progressSpinnerVisibility = true;
            var api = "https://2emmiqca42.execute-api.ap-south-1.amazonaws.com/prod/order/new/search";

            var date = new Date();
            date.setDate(date.getDate() - 1);

            var filter = {};
            filter.attributeName = "date";
            filter.operation = "GE";
            filter.attributeType = "NUMBER";
            filter.attributeValue = date.getTime();    // time in microseconds

            var data = {};
            data.filters = [filter];
            $http.post(api, data).then(function successCallback(response) {
                                          // this callback will be called asynchronously
                                          // when the response is available
                                          console.log(response);
                                          $scope.orders = response.data.orders;
                                          $scope.progressSpinnerVisibility = false;
                                        }, function errorCallback(response) {
                                          // called asynchronously if an error occurs
                                          // or server returns response with an error status.
                                          $scope.progressSpinnerVisibility = false;
                                          console.log("Could not fetch orders");
                                        });
        }
        fetchOrders();

        $scope.openOrder = function() {



        }


});