angular.module('navbarModule', []);

angular.module('navbarModule')
.component('navbarMenu', {
    templateUrl: "components/navigation.component.html",
    controller: function($scope) {
        $scope.tabs = $scope.$parent.tabs;
    }
});