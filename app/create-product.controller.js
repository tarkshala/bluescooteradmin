var blueScooterAdminModule = angular.module('bluescooter-admin');

blueScooterAdminModule.controller('productCtrl', function($scope, $http, $httpParamSerializerJQLike) {

    $scope.s3ServerUrl = "https://s3.ap-south-1.amazonaws.com/";

    var initialize = function() {
        $scope.name = "";
        $scope.photo = "";
        $scope.description = "";
        $scope.photoS3Url = null;
        $scope.category = "Choose Category";

        $('#successful-submission-alert').hide();
        $('#photo-upload-alert-success').hide();

        $('.alert').hide();

        $scope.product = {}

    }

    var fetchCategories = function() {
            var api = "https://2emmiqca42.execute-api.ap-south-1.amazonaws.com/prod/category";
            var data = {};
            data.entityType = "Category";
            $http.get(api, data).then(function successCallback(response) {
                                                          // this callback will be called asynchronously
                                                          // when the response is available
                                                          console.log(response);
                                                          $scope.categories = response.data.categories;
                                                        }, function errorCallback(response) {
                                                          // called asynchronously if an error occurs
                                                          // or server returns response with an error status.
                                                          console.log("Asafal");
                                                        });
    }

    initialize();
    fetchCategories();

    $scope.api = "https://mkieryyawj.execute-api.ap-south-1.amazonaws.com/prod";

    // AWS-S3 SDK
    AWS.config.region = 'ap-south-1'; // 1. Enter your region

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-south-1:d8b6e95a-f99a-4389-a9c5-e899cd37c455',
    });

    AWS.config.credentials.get(function(err) {
        if (err) alert(err);
        console.log(AWS.config.credentials);
    });

    var bucketName = 'blue-scooty-content'; // Enter your bucket name
    var bucket = new AWS.S3({
        params: {
            Bucket: bucketName
        }
    });

    var fileChooser = document.getElementById('photo');
    fileChooser.onchange = function(e) {
        $scope.photo = e.target.files[0].name;
        uploadImage();
        $scope.photoKey = constructImageKey();

    }

    var constructImageKey = function() {
        return $scope.photo + "." + (new Date()).getTime()+ "." + extractExtension();
    }

    var extractExtension = function() {
        return fileChooser.value.split(".").pop();
    }

    var uploadImage = function() {
        var file = fileChooser.files[0];
        var params = {
            Bucket: bucketName,
            Key: constructImageKey(),
            ContentType: file.type,
            Body: file
        };
        var s3 = new AWS.S3();
        s3.putObject(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else {
                $('#photo-upload-alert-success').show();
                setTimeout(function(){
                    $('#photo-upload-alert-success').hide();
                }, 5000)
                $scope.product.imageUrl = constructPhotoUrl();
            }
        });
    };

    var constructPhotoUrl = function() {
        if ($scope.photo) {
            return $scope.s3ServerUrl + bucketName + "/" + constructImageKey();
        }
        return null;
    };

    $scope.chooseCategory = function(category) {
        $scope.category = category.name;
        $scope.product.category = category;
    }

    $scope.updateTags = function() {
        $scope.product.tags = $('#tags').tagsinput('items');
    }

    $scope.submit = function() {
        console.log($scope.product)

        var api = "https://2emmiqca42.execute-api.ap-south-1.amazonaws.com/prod/product";
        data = {};

        data.productBO = $scope.product;

        $http.post(api, data).then(function successCallback(response) {
                                                      // this callback will be called asynchronously
                                                      // when the response is available
                                                      console.log(response);
                                                      clearForm();

                                                    }, function errorCallback(response) {
                                                      // called asynchronously if an error occurs
                                                      // or server returns response with an error status.
                                                      console.log("Asafal");
                                                    });
    }

    var clearForm = function() {
        $scope.product.name = null;
        fileChooser.value = "";
        $scope.product.description = null;
        $scope.product.longDescription = null;
        $scope.product.category = null;
        $scope.product.mrp = null;
        $scope.product.discount = null;
        $('#tags').tagsinput("removeAll");
    }


})