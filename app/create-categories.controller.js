var blueScooterAdminModule = angular.module('bluescooter-admin');

blueScooterAdminModule.controller('categoryCtrl', function($scope, $http, $httpParamSerializerJQLike) {

    $scope.name = "";
    $scope.photo = "";
    $scope.description = "";
    $scope.photoS3Url = null;
    $scope.s3ServerUrl = "https://s3.ap-south-1.amazonaws.com/";

    $scope.tabs = [{"name":"Categories"},
                   {"name":"Products"},
                    {"name":"Orders"}];

    $scope.api = "https://2emmiqca42.execute-api.ap-south-1.amazonaws.com/prod/category";
    $('#successful-submission-alert').hide();
    $('#photo-upload-alert-success').hide();

    // AWS-S3 SDK
    AWS.config.region = 'ap-south-1'; // 1. Enter your region

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-south-1:d8b6e95a-f99a-4389-a9c5-e899cd37c455',
    });

    AWS.config.credentials.get(function(err) {
        if (err) alert(err);
        console.log(AWS.config.credentials);
    });

    var bucketName = 'blue-scooty-content'; // Enter your bucket name
    var bucket = new AWS.S3({
        params: {
            Bucket: bucketName
        }
    });

    var fileChooser = document.getElementById('categoryPhoto');
    fileChooser.onchange = function() {
        $scope.photo = fileChooser.value.name;
        uploadImage();
    }

    var uploadImage = function() {
        var file = fileChooser.files[0];
        $scope.photo = file.name;
        var params = {
            Bucket: bucketName,
            Key: file.name,
            ContentType: file.type,
            Body: file
        };
        var s3 = new AWS.S3();
        s3.putObject(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else {
                $('#photo-upload-alert-success').show();
                setTimeout(function(){
                    $('#photo-upload-alert-success').hide();
                }, 5000)
            }
        });
    };

    var constructPhotoUrl = function() {
        if ($scope.photo) {
            return $scope.s3ServerUrl + bucketName + "/" + $scope.photo;
        }
        return null;
    }

    var validateCategoryData = function() {
        return constructPhotoUrl() && $scope.name && $scope.description;
    }

    $scope.submitData = function() {
        if (!validateCategoryData()) return;

        var data = {};
        data.categoryBO = {};
        data.categoryBO.name = $scope.name;
        data.categoryBO.description = $scope.description;
        data.categoryBO.imageUrl = constructPhotoUrl();

        $http.post($scope.api, data).then(function successCallback(response) {
                                              // this callback will be called asynchronously
                                              // when the response is available
                                              console.log("Safal hua");
                                              $('#successful-submission-alert').show();
                                              $scope.clearForm();
                                            }, function errorCallback(response) {
                                              // called asynchronously if an error occurs
                                              // or server returns response with an error status.
                                              console.log("Asafal");
                                            });

        console.log("Successful submission");
    }

    $scope.clearForm = function() {
        $scope.name = null;
        $scope.description = null;
        $scope.photo = null;
        fileChooser.value = null;
    }

});



