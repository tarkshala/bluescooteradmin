var blueScooterAdminModule = angular.module('bluescooter-admin');

blueScooterAdminModule.config(function($routeProvider){

    $routeProvider.when("/", {
        templateUrl : "html/categories.html",
    }).when("/order", {
        templateUrl : "html/orders.html"
    }).when("/product", {
        templateUrl : "html/product.html"
    });
});
